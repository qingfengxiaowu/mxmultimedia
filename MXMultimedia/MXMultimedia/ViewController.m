//
//  ViewController.m
//  MXMultimedia
//
//  Created by Qingfeng on 16/9/23.
//  Copyright © 2016年 Qingfeng. All rights reserved.
//

#import "ViewController.h"
#import "MXMVideoRecordingController.h"

@interface ViewController ()

@property (nonatomic, strong) UIButton *recordButton;

@end

@implementation ViewController

#pragma mark - Life Cycle

- (void)viewDidLoad {
    [super viewDidLoad];
    
    [self configureCustomViews];
}

#pragma mark - Event Response

- (void)didTapRecordButton:(UIButton *)button {
    MXMVideoRecordingController *controller = [[MXMVideoRecordingController alloc] initWithMinDuration:10 maxDuration:30];
    [self presentViewController:controller animated:YES completion:nil];
}

#pragma mark - Private

- (void)configureCustomViews {
    [self.view addSubview:self.recordButton];
    
    [self.recordButton mas_makeConstraints:^(MASConstraintMaker *make) {
        make.center.equalTo(self.view);
        make.size.mas_equalTo(CGSizeMake(160, 40));
    }];
}

#pragma mark - Getters & Setters

- (UIButton *)recordButton {
    if (_recordButton) {
        return _recordButton;
    }
    
    _recordButton = [UIButton buttonWithType:UIButtonTypeCustom];
    _recordButton.exclusiveTouch = YES;
    _recordButton.titleLabel.font = [UIFont systemFontOfSize:20];
    _recordButton.layer.borderColor = [UIColor orangeColor].CGColor;
    _recordButton.layer.borderWidth = 0.5;
    _recordButton.layer.cornerRadius = 4;
    [_recordButton setTitle:@"Start Record" forState:UIControlStateNormal];
    [_recordButton setTitleColor:[UIColor orangeColor] forState:UIControlStateNormal];
    [_recordButton addTarget:self
                      action:@selector(didTapRecordButton:)
            forControlEvents:UIControlEventTouchUpInside];
    return _recordButton;
}

@end
