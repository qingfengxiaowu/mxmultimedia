//
//  MXMCameraOverlayView.m
//  MXMultimedia
//
//  Created by Qingfeng on 16/9/24.
//  Copyright © 2016年 Qingfeng. All rights reserved.
//

#import "MXMCameraOverlayView.h"
#import <Masonry/Masonry.h>
#import "MXMRecordingProgressView.h"

typedef NS_ENUM(NSUInteger, MXMRecordingDeletePosition) {
    MXMRecordingDeletePositionLeft,
    MXMRecordingDeletePositionMiddle,
    MXMRecordingDeletePositionRight
};

static const CGFloat MXMRecordingCompletedRedundantDuration = 0.6;

@interface MXMCameraOverlayView ()

@property (nonatomic, strong) UIButton *backButton;
@property (nonatomic, strong) UIButton *recordButton;
@property (nonatomic, strong) UIButton *videoAdditionButton;
@property (nonatomic, strong) UIButton *completionButton;
@property (nonatomic, strong) UIButton *deleteButton;
@property (nonatomic, strong) MXMRecordingProgressView *progressView;

@end

@implementation MXMCameraOverlayView

#pragma mark - Life Cycle

- (instancetype)initWithMinDuration:(CGFloat)minDuration maxDuration:(CGFloat)maxDuration {
    self = [super init];
    if (self) {
        [self addProgressViewWithMinDuration:minDuration maxDuration:maxDuration];
        [self commonInitialization];
    }
    return self;
}

- (void)commonInitialization {
    [self addSubview:self.backButton];
    [self addSubview:self.recordButton];
    [self addSubview:self.videoAdditionButton];
    [self addSubview:self.completionButton];
    [self addSubview:self.deleteButton];
    
    [self.backButton mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(self).with.offset(40);
        make.centerY.equalTo(self.mas_bottom).with.offset(-77);
    }];
    [self.recordButton mas_makeConstraints:^(MASConstraintMaker *make) {
        make.centerX.equalTo(self);
        make.centerY.equalTo(self.backButton);
    }];
    [self.videoAdditionButton mas_makeConstraints:^(MASConstraintMaker *make) {
        make.right.equalTo(self).with.offset(-40);
        make.centerY.equalTo(self.backButton);
    }];
    [self.completionButton mas_makeConstraints:^(MASConstraintMaker *make) {
        make.right.equalTo(self).with.offset(-40);
        make.centerY.equalTo(self.backButton);
    }];
    [self.deleteButton mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(self.progressView.mas_bottom).with.offset(5);
        make.centerX.equalTo(self.mas_left);
    }];
    
    self.completionButton.hidden = YES;
    UITapGestureRecognizer *recognizer = [[UITapGestureRecognizer alloc] initWithTarget:self
                                                                                 action:@selector(didTapCameraOverlayView:)];
    recognizer.numberOfTapsRequired = 2;
    [self addGestureRecognizer:recognizer];
}

#pragma mark - Event Response

- (void)didTapCameraOverlayView:(UITapGestureRecognizer *)recognizer {
    if ([self.delegate respondsToSelector:@selector(didToggleCameraSwitching:)]) {
        [self.delegate didToggleCameraSwitching:self];
    }
}

- (void)didTapBackButton:(UIButton *)button {
    if ([self.delegate respondsToSelector:@selector(overlayView:didTapBackButton:)]) {
        [self.delegate overlayView:self didTapBackButton:button];
    }
}

- (void)didTapRecordButton:(UIButton *)button {
    button.selected = !button.selected;
    if (button.selected) {
        if (self.deleteButton.selected) {
            self.progressView.deletionSelected = self.deleteButton.selected = NO;
        }
        self.deleteButton.hidden = YES;
    }
    
    if ([self.delegate respondsToSelector:@selector(overlayView:didTapRecordButton:)]) {
        [self.delegate overlayView:self didTapRecordButton:button];
    }
}

- (void)didTapVideoAdditionButton:(UIButton *)button {
    if ([self.delegate respondsToSelector:@selector(overlayView:didTapVideoAdditionButton:)]) {
        [self.delegate overlayView:self didTapVideoAdditionButton:button];
    }
}

- (void)didTapCompletionButton:(UIButton *)button {
    if ([self.delegate respondsToSelector:@selector(overlayView:didTapcompletionButton:)]) {
        [self.delegate overlayView:self didTapcompletionButton:button];
    }
}

- (void)didTapDeleteButton:(UIButton *)button {
    if (button.selected) {
        button.selected = NO;
        if ([self.delegate respondsToSelector:@selector(didToggleVideoDeletion:)]) {
            [self.delegate didToggleVideoDeletion:self];
        }
    }
    else  {
        self.progressView.deletionSelected = button.selected = YES;
    }
}

#pragma mark - Public

- (void)updateWithCompletedDurationValues:(NSArray<NSNumber *> *)completedDurationValues {
    [self.progressView updateWithCompletedDurationValues:completedDurationValues];
    [self layoutDeleteButton];
    [self updateButtonStates:YES];
}

- (void)updateWithCurrentDuration:(CGFloat)currentDuration {
    [self.progressView updateWithCurrentDuration:currentDuration];
    [self updateButtonStates:NO];
}

#pragma mark - Private

- (void)addProgressViewWithMinDuration:(CGFloat)minDuration maxDuration:(CGFloat)maxDuration {
    MXMRecordingProgressView *progressView = [[MXMRecordingProgressView alloc] initWithMinDuration:minDuration maxDuration:maxDuration];
    self.progressView = progressView;
    [self addSubview:progressView];
    [progressView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.left.and.right.equalTo(self);
        make.height.mas_equalTo(20);
    }];
}

- (void)layoutDeleteButton {
    CGFloat totalDuration = self.progressView.totalCompletedDuration;
    CGFloat screenWidth = CGRectGetWidth([UIScreen mainScreen].bounds);
    CGFloat totalWidth = screenWidth * totalDuration / self.progressView.maxDuration;
    MXMRecordingDeletePosition position = MXMRecordingDeletePositionMiddle;
    CGFloat comparedWidth = [self.deleteButton imageForState:UIControlStateNormal].size.width / 2;
    if (totalWidth < comparedWidth) {
        position = MXMRecordingDeletePositionLeft;
    }
    else if (screenWidth - totalWidth < comparedWidth) {
        position = MXMRecordingDeletePositionRight;
    }
    UIImage *normalImage = [self recordingDeleteNormalImageWithPosition:position];
    UIImage *selectedImage = [self recordingDeleteSelectedImageWithPosition:position];
    [self.deleteButton setImage:normalImage forState:UIControlStateNormal];
    [self.deleteButton setImage:selectedImage forState:UIControlStateHighlighted];
    [self.deleteButton setImage:selectedImage forState:UIControlStateSelected];
    
    if (totalDuration > pow(10, -6)) {
        self.deleteButton.hidden = NO;
        switch (position) {
            case MXMRecordingDeletePositionLeft: {
                [self.deleteButton mas_remakeConstraints:^(MASConstraintMaker *make) {
                    make.top.equalTo(self.progressView.mas_bottom).with.offset(5);
                    make.left.equalTo(self).with.offset(totalWidth + 1.5);
                }];
            }
                break;
            case MXMRecordingDeletePositionMiddle: {
                [self.deleteButton mas_remakeConstraints:^(MASConstraintMaker *make) {
                    make.top.equalTo(self.progressView.mas_bottom).with.offset(5);
                    make.centerX.equalTo(self.mas_left).with.offset(totalWidth + 1.5);
                }];
            }
                break;
            case MXMRecordingDeletePositionRight: {
                [self.deleteButton mas_remakeConstraints:^(MASConstraintMaker *make) {
                    make.top.equalTo(self.progressView.mas_bottom).with.offset(5);
                    make.right.equalTo(self).with.offset(-(screenWidth - totalWidth - 1.5));
                }];
            }
                break;
        }
    }
    else {
        self.deleteButton.hidden = YES;
    }
}

- (void)updateButtonStates:(BOOL)completed {
    CGFloat totalDuration = self.progressView.totalCompletedDuration;
    if (!completed) {
        totalDuration += self.progressView.currentDuration;
    }
    
    CGFloat comparedNumber = pow(10, -6);
    self.videoAdditionButton.hidden = totalDuration > comparedNumber;
    self.completionButton.hidden = !self.videoAdditionButton.hidden;
    self.completionButton.selected = totalDuration > self.progressView.minDuration;
    if (completed) {
        if (fabs(totalDuration - self.progressView.maxDuration) < MXMRecordingCompletedRedundantDuration ||
            totalDuration >= self.progressView.maxDuration) {
            self.recordButton.enabled = NO;
        }
        else  {
            self.recordButton.enabled = YES;
        }
    }
    else {
        if (fabs(totalDuration - self.progressView.maxDuration) < comparedNumber ||
            totalDuration >= self.progressView.maxDuration) {
            [self didTapRecordButton:self.recordButton];
            self.recordButton.enabled = NO;
        }
        else {
            self.recordButton.enabled = YES;
        }
    }
}

- (UIButton *)buttonWithNormalImageName:(NSString *)normalImageName
                      selectedImageName:(NSString *)selectedImageName
                               selector:(SEL)selector {
    UIButton *button = [UIButton buttonWithType:UIButtonTypeCustom];
    button.exclusiveTouch = YES;
    if (!!normalImageName.length) {
        [button setImage:[UIImage imageNamed:normalImageName] forState:UIControlStateNormal];
        [button setImage:[UIImage imageNamed:normalImageName] forState:UIControlStateDisabled];
    }
    if (!!selectedImageName.length) {
        [button setImage:[UIImage imageNamed:selectedImageName] forState:UIControlStateHighlighted];
        [button setImage:[UIImage imageNamed:selectedImageName] forState:UIControlStateSelected];
    }
    [button addTarget:self action:selector forControlEvents:UIControlEventTouchUpInside];
    
    return button;
}

- (UIImage *)recordingDeleteNormalImageWithPosition:(MXMRecordingDeletePosition)position {
    UIImage *image = nil;
    switch (position) {
        case MXMRecordingDeletePositionLeft:
            image = [UIImage imageNamed:@"MXMRecordingMarginDeleteNormal.png"];
            break;
        case MXMRecordingDeletePositionMiddle:
            image = [UIImage imageNamed:@"MXMRecordingDeleteNormal.png"];
            break;
        case MXMRecordingDeletePositionRight: {
            image = [UIImage imageNamed:@"MXMRecordingMarginDeleteNormal.png"];
            image = [UIImage imageWithCGImage:image.CGImage scale:image.scale orientation:UIImageOrientationUpMirrored];
        }
            break;
    }
    return image;
}

- (UIImage *)recordingDeleteSelectedImageWithPosition:(MXMRecordingDeletePosition)position {
    UIImage *image = nil;
    switch (position) {
        case MXMRecordingDeletePositionLeft:
            image = [UIImage imageNamed:@"MXMRecordingMarginDeleteSelected.png"];
            break;
        case MXMRecordingDeletePositionMiddle:
            image = [UIImage imageNamed:@"MXMRecordingDeleteSelected.png"];
            break;
        case MXMRecordingDeletePositionRight: {
            image = [UIImage imageNamed:@"MXMRecordingMarginDeleteSelected.png"];
            image = [UIImage imageWithCGImage:image.CGImage scale:image.scale orientation:UIImageOrientationUpMirrored];
        }
            break;
    }
    return image;
}

#pragma mark - Getters & Setters

- (UIButton *)backButton {
    if (_backButton) {
        return _backButton;
    }
    
    _backButton = [self buttonWithNormalImageName:@"MXMCameraBack.png"
                                selectedImageName:nil
                                         selector:@selector(didTapBackButton:)];
    return _backButton;
}

- (UIButton *)recordButton {
    if (_recordButton) {
        return _recordButton;
    }
    
    _recordButton = [self buttonWithNormalImageName:@"MXMVideoRecordingNormal.png"
                                  selectedImageName:@"MXMVideoRecordingPaused.png"
                                           selector:@selector(didTapRecordButton:)];
    [_recordButton setImage:[UIImage imageNamed:@"MXMVideoRecordingDisabled.png"]
                   forState:UIControlStateDisabled];
    return _recordButton;
}

- (UIButton *)videoAdditionButton {
    if (_videoAdditionButton) {
        return _videoAdditionButton;
    }
    
    _videoAdditionButton = [self buttonWithNormalImageName:@"MXMVideoAddition.png"
                                         selectedImageName:nil
                                                  selector:@selector(didTapVideoAdditionButton:)];
    return _videoAdditionButton;
}

- (UIButton *)completionButton {
    if (_completionButton) {
        return _completionButton;
    }
    
    _completionButton = [self buttonWithNormalImageName:@"MXMRecordingCompletionInvalid.png"
                                      selectedImageName:@"MXMRecordingCompletionValid.png"
                                               selector:@selector(didTapCompletionButton:)];
    return _completionButton;
}

- (UIButton *)deleteButton {
    if (_deleteButton) {
        return _deleteButton;
    }
    
    _deleteButton = [self buttonWithNormalImageName:@"MXMRecordingDeleteNormal.png"
                                  selectedImageName:@"MXMRecordingDeleteSelected.png"
                                           selector:@selector(didTapDeleteButton:)];
    return _deleteButton;
}

@end
