//
//  MXMCameraOverlayView.h
//  MXMultimedia
//
//  Created by Qingfeng on 16/9/24.
//  Copyright © 2016年 Qingfeng. All rights reserved.
//

#import <UIKit/UIKit.h>

///--------------------------------
/// @name Class MXMCameraOverlayView
///--------------------------------

@protocol MXMCameraOverlayViewDelegate;

@interface MXMCameraOverlayView : UIView

@property (nonatomic, weak) id<MXMCameraOverlayViewDelegate> delegate;

- (instancetype)initWithMinDuration:(CGFloat)minDuration maxDuration:(CGFloat)maxDuration;

- (void)updateWithCompletedDurationValues:(NSArray<NSNumber *> *)completedDurationValues;

- (void)updateWithCurrentDuration:(CGFloat)currentDuration;

@end

///-------------------------------------------
/// @name Protocol MXMCameraOverlayViewDelegate
///-------------------------------------------

@protocol MXMCameraOverlayViewDelegate <NSObject>

@optional
- (void)didToggleCameraSwitching:(MXMCameraOverlayView *)overlayView;
- (void)didToggleVideoDeletion:(MXMCameraOverlayView *)overlayView;
- (void)overlayView:(MXMCameraOverlayView *)overlayView didTapBackButton:(UIButton *)button;
- (void)overlayView:(MXMCameraOverlayView *)overlayView didTapRecordButton:(UIButton *)button;
- (void)overlayView:(MXMCameraOverlayView *)overlayView didTapVideoAdditionButton:(UIButton *)button;
- (void)overlayView:(MXMCameraOverlayView *)overlayView didTapcompletionButton:(UIButton *)button;

@end
