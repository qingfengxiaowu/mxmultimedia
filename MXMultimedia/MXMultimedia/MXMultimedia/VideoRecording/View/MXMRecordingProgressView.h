//
//  MXMRecordingProgressView.h
//  MXMultimedia
//
//  Created by Qingfeng on 16/9/24.
//  Copyright © 2016年 Qingfeng. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface MXMRecordingProgressView : UIView

@property (nonatomic, readonly) CGFloat minDuration;
@property (nonatomic, readonly) CGFloat maxDuration;
@property (nonatomic, readonly) CGFloat currentDuration;
@property (nonatomic, readonly, copy) NSArray<NSNumber *> *completedDurationValues;
@property (nonatomic, readonly) CGFloat totalCompletedDuration;
@property (nonatomic) BOOL deletionSelected;

- (instancetype)initWithMinDuration:(CGFloat)minDuration maxDuration:(CGFloat)maxDuration;

- (void)updateWithCompletedDurationValues:(NSArray<NSNumber *> *)completedDurationValues;

- (void)updateWithCurrentDuration:(CGFloat)currentDuration;

@end
