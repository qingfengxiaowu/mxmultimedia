//
//  MXMRecordingProgressView.m
//  MXMultimedia
//
//  Created by Qingfeng on 16/9/24.
//  Copyright © 2016年 Qingfeng. All rights reserved.
//

#import "MXMRecordingProgressView.h"
#import <Masonry/Masonry.h>
#import "UIColor+MXM.h"

#define MXMCommonLineWidth  (1.0 / [UIScreen mainScreen].scale)
#define MXMCompletedDurationColor [UIColor mxm_colorWithHexString:@"#ff6600"]
#define MXMCompletedDeletionColor [UIColor mxm_colorWithHexString:@"#ff0000"]

@interface MXMCompletedDurationView : UIView

@property (nonatomic, strong) UIView *durationView;
@property (nonatomic, strong) UIView *maskView;
@property (nonatomic) BOOL deletionSelected;

@end

@implementation MXMCompletedDurationView

- (instancetype)initWithFrame:(CGRect)frame {
    self = [super initWithFrame:frame];
    if (self) {
        [self addSubview:self.durationView];
        [self addSubview:self.maskView];
        
        [self.durationView mas_makeConstraints:^(MASConstraintMaker *make) {
            make.edges.equalTo(self).with.insets(UIEdgeInsetsMake(0, 0, 0, MXMCommonLineWidth));
        }];
        [self.maskView mas_makeConstraints:^(MASConstraintMaker *make) {
            make.edges.equalTo(self);
        }];
    }
    return self;
}

- (void)setDeletionSelected:(BOOL)deletionSelected {
    _deletionSelected = deletionSelected;
    self.maskView.backgroundColor = _deletionSelected ? MXMCompletedDeletionColor : [UIColor clearColor];
}

- (UIView *)durationView {
    if (_durationView) {
        return _durationView;
    }
    
    _durationView = [UIView new];
    _durationView.backgroundColor = MXMCompletedDurationColor;
    return _durationView;
}

- (UIView *)maskView {
    if (_maskView) {
        return _maskView;
    }
    
    _maskView = [UIView new];
    return _maskView;
}

@end

static const NSInteger MXMRecordingDefaultMaxDuration = 60;

@interface MXMRecordingProgressView ()

@property (nonatomic, strong) UIView *minSeparatorView;
@property (nonatomic, strong) UIView *currentDurationView;
@property (nonatomic, strong) UIImageView *recordingIndicatorView;
@property (nonatomic, strong) UILabel *durationLabel;
@property (nonatomic, readonly, strong) MXMCompletedDurationView *lastCompletedDurationView;
@property (nonatomic, copy) NSArray<MXMCompletedDurationView *> *completedDurationViews;

@end

@implementation MXMRecordingProgressView

@dynamic deletionSelected, lastCompletedDurationView, totalCompletedDuration;

#pragma mark - Life Cycle

- (instancetype)initWithMinDuration:(CGFloat)minDuration maxDuration:(CGFloat)maxDuration {
    self = [super init];
    if (self) {
        _minDuration = minDuration;
        _maxDuration = maxDuration > 0 ? maxDuration : MXMRecordingDefaultMaxDuration;
        [self commonInitialization];
    }
    return self;
}

- (void)commonInitialization {
    self.backgroundColor = [[UIColor blackColor] colorWithAlphaComponent:0.3];
    
    [self addSubview:self.minSeparatorView];
    [self addSubview:self.durationLabel];
    [self.minSeparatorView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.and.bottom.equalTo(self);
        make.left.equalTo(self.mas_right).multipliedBy(self.minDuration / self.maxDuration);
        make.width.mas_equalTo(1);
    }];
    [self.durationLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.center.equalTo(self);
        make.size.mas_equalTo(CGSizeMake(100, 20));
    }];
}

#pragma mark - Configuring Data

- (void)updateWithCompletedDurationValues:(NSArray<NSNumber *> *)completedDurationValues {
    [self.currentDurationView removeFromSuperview];
    if (!completedDurationValues.count) {
        for (UIView *view in self.completedDurationViews) {
            [view removeFromSuperview];
        }
        self.completedDurationViews = nil;
        _completedDurationValues = completedDurationValues;
        self.durationLabel.text = [self displayDuration:YES];
        [self configureRecordingIndicatorView];
        return;
    }
    
    BOOL sameDurationValues = YES;
    if (self.completedDurationValues.count != completedDurationValues.count) {
        sameDurationValues = NO;
    }
    else {
        for (NSInteger index = 0; index < completedDurationValues.count; ++index) {
            NSNumber *durationValue = self.completedDurationValues[index];
            NSNumber *newDurationValue = completedDurationValues[index];
            if (fabs(durationValue.doubleValue - newDurationValue.doubleValue) > pow(10, -6)) {
                sameDurationValues = NO;
                break;
            }
        }
    }
    if (sameDurationValues) {
        return;
    }
    
    _completedDurationValues = completedDurationValues;
    self.durationLabel.text = [self displayDuration:YES];
    
    for (UIView *view in self.completedDurationViews) {
        [view removeFromSuperview];
    }
    self.completedDurationViews = nil;
    [self addCompletedDurationViews];
    [self configureRecordingIndicatorView];
}

- (void)updateWithCurrentDuration:(CGFloat)currentDuration {
    _currentDuration = currentDuration;
    
    if (!self.currentDurationView) {
        UIView *currentDurationView = [UIView new];
        currentDurationView.backgroundColor = MXMCompletedDurationColor;
        self.currentDurationView = currentDurationView;
    }
    if (!self.currentDurationView.superview) {
        [self insertSubview:self.currentDurationView belowSubview:self.durationLabel];
    }
    [self.currentDurationView mas_remakeConstraints:^(MASConstraintMaker *make) {
        make.top.and.bottom.equalTo(self);
        make.width.equalTo(self).multipliedBy(currentDuration / self.maxDuration);
        if (self.lastCompletedDurationView) {
            make.left.equalTo(self.lastCompletedDurationView.mas_right);
        }
        else {
            make.left.equalTo(self);
        }
    }];
    
    if (!self.recordingIndicatorView.superview) {
        [self addSubview:self.recordingIndicatorView];
        [self bringSubviewToFront:self.recordingIndicatorView];
    }
    [self.recordingIndicatorView mas_remakeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(self);
        make.left.equalTo(self.currentDurationView.mas_right);
    }];
    
    self.durationLabel.text = [self displayDuration:NO];
}

#pragma mark - Private

- (void)addCompletedDurationViews {
    NSMutableArray *completedDurationViews = [NSMutableArray array];
    UIView *lastView = nil;
    for (NSNumber *durationValue in self.completedDurationValues) {
        MXMCompletedDurationView *durationView = [MXMCompletedDurationView new];
        [self insertSubview:durationView belowSubview:self.durationLabel];
        [completedDurationViews addObject:durationView];
        [durationView mas_makeConstraints:^(MASConstraintMaker *make) {
            make.top.and.bottom.equalTo(self);
            make.width.equalTo(self).multipliedBy(durationValue.doubleValue / self.maxDuration);
            if (lastView) {
                make.left.equalTo(lastView.mas_right);
            }
            else {
                make.left.equalTo(self);
            }
        }];
        lastView = durationView;
    }
    self.completedDurationViews = [completedDurationViews copy];
}

- (void)configureRecordingIndicatorView {
    if (!self.recordingIndicatorView.superview) {
        [self addSubview:self.recordingIndicatorView];
    }
    [self bringSubviewToFront:self.recordingIndicatorView];
    [self.recordingIndicatorView mas_remakeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(self);
        UIView *lastView = [self lastCompletedDurationView];
        if (lastView) {
            make.left.equalTo(lastView.mas_right);
        }
        else {
            make.left.equalTo(self);
        }
    }];
}

- (NSString *)displayDuration:(BOOL)completed {
    CGFloat totalDuration = self.totalCompletedDuration;
    if (!completed) {
        totalDuration += self.currentDuration;
    }
    
    NSUInteger tempSeconds = 10 * totalDuration;
    NSUInteger minutes = floor(tempSeconds / 600);
    NSUInteger seconds = floor(tempSeconds % 600 / 10);
    NSUInteger milliseconds = floor(tempSeconds % 10);
    NSString *formatString = nil;
    if (minutes > 0) {
        formatString = [NSString stringWithFormat:@"%02lu:%02lu.%lu秒", (unsigned long)minutes, (unsigned long)seconds, (unsigned long)milliseconds];
    }
    else {
        formatString = [NSString stringWithFormat:@"%02lu.%lu秒", (unsigned long)seconds, (unsigned long)milliseconds];
    }
    
    return formatString;
}

#pragma mark - Getters & Setters

- (UIView *)minSeparatorView {
    if (_minSeparatorView) {
        return _minSeparatorView;
    }
    
    _minSeparatorView = [UIView new];
    _minSeparatorView.backgroundColor = [UIColor whiteColor];
    return _minSeparatorView;
}

- (UIImageView *)recordingIndicatorView {
    if (_recordingIndicatorView) {
        return _recordingIndicatorView;
    }
    
    UIImage *image = [UIImage imageNamed:@"MXMRecordingIndicator.png"];
    _recordingIndicatorView = [[UIImageView alloc] initWithImage:image];
    return _recordingIndicatorView;
}

- (UILabel *)durationLabel {
    if (_durationLabel) {
        return _durationLabel;
    }
    
    _durationLabel = [UILabel new];
    _durationLabel.font = [UIFont systemFontOfSize:11];
    _durationLabel.textAlignment = NSTextAlignmentCenter;
    _durationLabel.textColor = [UIColor whiteColor];
    _durationLabel.shadowColor = [[UIColor blackColor] colorWithAlphaComponent:0.45];
    _durationLabel.shadowOffset = CGSizeMake(0, 1);
    return _durationLabel;
}

- (MXMCompletedDurationView *)lastCompletedDurationView {
    return self.completedDurationViews.lastObject;
}

- (CGFloat)totalCompletedDuration {
    CGFloat totalDuration = 0;
    for (NSNumber *completedDurationValue in self.completedDurationValues) {
        totalDuration += completedDurationValue.doubleValue;
    }
    return totalDuration;
}

- (BOOL)deletionSelected {
    MXMCompletedDurationView *lastCompletedDurationView = self.lastCompletedDurationView;
    return lastCompletedDurationView.deletionSelected;
}

- (void)setDeletionSelected:(BOOL)deletionSelected {
    MXMCompletedDurationView *lastCompletedDurationView = self.lastCompletedDurationView;
    lastCompletedDurationView.deletionSelected = deletionSelected;
}

@end
