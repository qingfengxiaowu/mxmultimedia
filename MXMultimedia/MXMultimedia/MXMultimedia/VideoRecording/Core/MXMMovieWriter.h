//
//  MXMMovieWriter.h
//  MXMultimedia
//
//  Created by Qingfeng on 16/9/23.
//  Copyright © 2016年 Qingfeng. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreMedia/CoreMedia.h>

@protocol MXMMovieWriterDelegate;

@interface MXMMovieWriter : NSObject

- (id)initWithDelegate:(id<MXMMovieWriterDelegate>)delegate;

- (void) processFrame:(CMSampleBufferRef)sampleBuffer isVideo:(BOOL)isVideo;

- (BOOL) finishWithCompletionHandler:(void (^)(void))handler;

@property (nonatomic, weak) id<MXMMovieWriterDelegate> delegate;
@property (nonatomic, assign) CGAffineTransform videoInputTransform;
@property (nonatomic, assign) BOOL isRecording;
@property (nonatomic, assign, readonly) CMTime duration;

- (void)startTakeMovieWithPath:(NSString *)moviePath;
- (void) stopTakeMovie;
- (void) cancelTakeMovie;

@end

@protocol MXMMovieWriterDelegate <NSObject>
@optional
- (void)movieWriterDidFinishRecordedFileURL:(NSURL *)outputFileURL ;
- (void)movieWriterDidFinishFileURL:(NSURL *)fileURL thumbImage:(UIImage *)thumbImage lastImage:(UIImage *)lastImage;
@end
