//
//  MXMMovieWriter.m
//  MXMultimedia
//
//  Created by Qingfeng on 16/9/23.
//  Copyright © 2016年 Qingfeng. All rights reserved.
//

#import "MXMMovieWriter.h"
#import <AVFoundation/AVFoundation.h>

@interface MXMMovieWriter()
{
    AVAssetWriter *_writer;
    AVAssetWriterInput *_videoInput;
    AVAssetWriterInput *_audioInput;
    NSURL *_path;
    
    
    CMSampleBufferRef _firstVideoFrameBuffer;
    CMSampleBufferRef _lastVideoFrameBuffer;
    
    CMTime _startTime;
    CMTime _currentTime;
    BOOL _isRecording;
    BOOL _setSessionStart;
}
@property (nonatomic, strong) NSURL *path;
@property (nonatomic, strong) NSDictionary *videoSetting;
@property (nonatomic, strong) NSDictionary *audioSetting;

@end

@implementation MXMMovieWriter
@synthesize isRecording = _isRecording;
@synthesize path = _path;

- (void)dealloc
{
    if (_firstVideoFrameBuffer)
    {
        CFRelease(_firstVideoFrameBuffer);
    }
    if (_lastVideoFrameBuffer)
    {
        CFRelease(_lastVideoFrameBuffer);
    }
}

- (id)initWithDelegate:(id<MXMMovieWriterDelegate>)delegate
{
    self = [super init];
    if (self) {
        self.delegate = delegate;
        _firstVideoFrameBuffer = NULL;
        _lastVideoFrameBuffer = NULL;
    }
    return self;
}

- (void)setupWriter
{
    [[NSFileManager defaultManager]  removeItemAtURL:self.path error:nil];
    
    _writer = [[AVAssetWriter alloc] initWithURL:self.path fileType:AVFileTypeQuickTimeMovie error:nil];
    
    _audioInput = [AVAssetWriterInput assetWriterInputWithMediaType:AVMediaTypeAudio outputSettings:self.audioSetting];
    _audioInput.expectsMediaDataInRealTime = YES;
    
    _videoInput = [AVAssetWriterInput assetWriterInputWithMediaType:AVMediaTypeVideo outputSettings:self.videoSetting];
    _videoInput.expectsMediaDataInRealTime = YES;
    _videoInput.transform = self.videoInputTransform;
    
    if ([_writer canAddInput:_audioInput]) {
        [_writer addInput:_audioInput];
    }
    
    if ([_writer canAddInput:_videoInput]) {
        [_writer addInput:_videoInput];
    }
    [_writer startWriting];
    _setSessionStart = NO;
}

- (void)cleanWriter
{
    _writer = nil;
}

- (CMTime)duration
{
    if( ! CMTIME_IS_VALID(_startTime) )
        return kCMTimeZero;
    if( ! CMTIME_IS_NEGATIVE_INFINITY(_currentTime) )
        return CMTimeSubtract(_currentTime, _startTime);
    return kCMTimeZero;
}

- (void)startTakeMovieWithPath:(NSString *)moviePath;
{
    if (_isRecording) {
        return;
    }
    
    self.path = [NSURL fileURLWithPath:moviePath];
    [self setupWriter];
    
    _isRecording = YES;
    _startTime = kCMTimeInvalid;
    _currentTime = kCMTimeInvalid;
}

- (void) stopTakeMovie
{
    _isRecording = NO;
    [self finishWithCompletionHandler:^{
        NSLog(@"<==finishWithCompletionHandler");
        NSLog(@"finishWithCompletionHandler");
    }];
}

- (void) cancelTakeMovie
{
    _isRecording = NO;
    [_writer cancelWriting];
    [self cleanWriter];
}

- (BOOL) finishWithCompletionHandler:(void (^)(void))handler
{
    if (_writer.status == AVAssetWriterStatusCompleted || _writer.status == AVAssetWriterStatusCancelled || _writer.status == AVAssetWriterStatusUnknown || CMTIME_IS_INVALID(_currentTime))
    {
        handler();
        NSLog(@"finishWithCompletionHandler error");
        return NO;
    }
    [_videoInput markAsFinished];
    [_audioInput markAsFinished];
    if (CMTIME_COMPARE_INLINE(_currentTime, !=, kCMTimeZero))
    {
        [_writer endSessionAtSourceTime:_currentTime];
    }
    
    [_writer finishWritingWithCompletionHandler:^{
        handler();
        if (self.delegate && [self.delegate respondsToSelector:@selector(movieWriterDidFinishFileURL:thumbImage:lastImage:)]) {
            UIImage *thumbImage = nil;
            if (_firstVideoFrameBuffer) {
                thumbImage = [self imageFromSampleBuffer:_firstVideoFrameBuffer];
                CFRelease(_firstVideoFrameBuffer);  _firstVideoFrameBuffer = NULL;
            }
            UIImage *lastImage = nil;
            if (_lastVideoFrameBuffer) {
                lastImage = [self imageFromSampleBuffer:_lastVideoFrameBuffer];
                CFRelease(_lastVideoFrameBuffer);   _lastVideoFrameBuffer = NULL;
            }
            [self.delegate movieWriterDidFinishFileURL:self.path thumbImage:thumbImage lastImage:lastImage];
        }
    }];
    return YES;
}

- (void) processFrame:(CMSampleBufferRef)sampleBuffer isVideo:(BOOL)isVideo
{
    [self configInputWithSampleBuffer:sampleBuffer isVideo:isVideo];
    
    if (_isRecording) {
        [self encodeFrame:sampleBuffer isVideo:isVideo];
    }
}

- (BOOL) encodeFrame:(CMSampleBufferRef) sampleBuffer isVideo:(BOOL)bVideo
{
    if (CMSampleBufferDataIsReady(sampleBuffer))
    {
        if (!_setSessionStart) {
            // 保证第一帧为视频帧
            if (!bVideo) { return NO; }
            
            [_writer startSessionAtSourceTime:CMSampleBufferGetPresentationTimeStamp(sampleBuffer)];
            _setSessionStart = YES;
        }
        if (_writer.status == AVAssetWriterStatusFailed)
        {
            NSLog(@"writer error %@", _writer.error.localizedDescription);
            return NO;
        }
        if (bVideo)
        {
            CMTime timestamp = CMSampleBufferGetPresentationTimeStamp(sampleBuffer);
            if (CMTIME_IS_INVALID(_startTime))
            {
                _startTime = timestamp;
                _currentTime = timestamp;
            }
            else
            {
                _currentTime = timestamp;
            }
            if (_videoInput.readyForMoreMediaData == YES)
            {
                [_videoInput appendSampleBuffer:sampleBuffer];
                
                if (_firstVideoFrameBuffer == NULL) {
                    _firstVideoFrameBuffer = sampleBuffer;
                    CFRetain(_firstVideoFrameBuffer);
                }
                if (_lastVideoFrameBuffer != NULL)
                {
                    CFRelease(_lastVideoFrameBuffer);
                    _lastVideoFrameBuffer = NULL;
                }
                _lastVideoFrameBuffer = sampleBuffer;
                CFRetain(_lastVideoFrameBuffer);
                return YES;
            }
        }
        else
        {
            if (_audioInput.readyForMoreMediaData)
            {
                [_audioInput appendSampleBuffer:sampleBuffer];
                return YES;
            }
        }
    }
    return NO;
}

- (void)configInputWithSampleBuffer:(CMSampleBufferRef)sampleBuffer isVideo:(BOOL)isVideo
{
    if (self.videoSetting && self.audioSetting) {
        return;
    }
    
    if ((_videoSetting == nil) && isVideo) {
        [self _configVideoInputWithSampleBuffer:sampleBuffer];
    }
    if ((_audioSetting == nil) && !isVideo) {
        [self _configAudioInputWithSampleBuffer:sampleBuffer];
    }
}

#pragma mark - config video & audio setting
- (void)_configAudioInputWithSampleBuffer:(CMSampleBufferRef)sampleBuffer
{
    CMFormatDescriptionRef formatDescription = CMSampleBufferGetFormatDescription(sampleBuffer);
    
    const AudioStreamBasicDescription *asbd = CMAudioFormatDescriptionGetStreamBasicDescription(formatDescription);
    if (!asbd) {
        return;
    }
    
    unsigned int channels = asbd->mChannelsPerFrame;
    double sampleRate = asbd->mSampleRate;
    
    size_t aclSize = 0;
    const AudioChannelLayout *currentChannelLayout = CMAudioFormatDescriptionGetChannelLayout(formatDescription, &aclSize);
    NSData *currentChannelLayoutData = ( currentChannelLayout && aclSize > 0 ) ? [NSData dataWithBytes:currentChannelLayout length:aclSize] : [NSData data];
    
    self.audioSetting = @{ AVFormatIDKey : @(kAudioFormatMPEG4AAC),
                           AVNumberOfChannelsKey : @(channels),
                           AVSampleRateKey :  @(sampleRate),
                           AVEncoderBitRateKey : @(64000),
                           AVChannelLayoutKey : currentChannelLayoutData };
    
}

- (void)_configVideoInputWithSampleBuffer:(CMSampleBufferRef)sampleBuffer
{
    CMFormatDescriptionRef formatDescription = CMSampleBufferGetFormatDescription(sampleBuffer);
    CMVideoDimensions dimensions = CMVideoFormatDescriptionGetDimensions(formatDescription);
    
    NSDictionary *compressionSettings = @{ AVVideoAverageBitRateKey : @(5*1024*1024),
                                           AVVideoMaxKeyFrameIntervalKey : @(30) };
    
    self.videoSetting = @{ AVVideoCodecKey : AVVideoCodecH264,
                           AVVideoWidthKey : @(dimensions.width),
                           AVVideoHeightKey : @(dimensions.height),
                           AVVideoCompressionPropertiesKey : compressionSettings };
    
}

- (UIImage *) imageFromSampleBuffer:(CMSampleBufferRef) sampleBuffer
{
    CVImageBufferRef imageBuffer = CMSampleBufferGetImageBuffer(sampleBuffer);
    CVPixelBufferLockBaseAddress(imageBuffer, 0);
    void *baseAddress = CVPixelBufferGetBaseAddress(imageBuffer);
    size_t bytesPerRow = CVPixelBufferGetBytesPerRow(imageBuffer);
    size_t width = CVPixelBufferGetWidth(imageBuffer);
    size_t height = CVPixelBufferGetHeight(imageBuffer);
    CGColorSpaceRef colorSpace = CGColorSpaceCreateDeviceRGB();
    
    CGContextRef context = CGBitmapContextCreate(baseAddress, width, height, 8,
                                                 bytesPerRow, colorSpace, kCGBitmapByteOrder32Little | kCGImageAlphaPremultipliedFirst);
    CGImageRef quartzImage = CGBitmapContextCreateImage(context);
    CVPixelBufferUnlockBaseAddress(imageBuffer,0);
    CGContextRelease(context);
    CGColorSpaceRelease(colorSpace);
    
    UIImageOrientation orientation = UIImageOrientationUp;
    if (CGAffineTransformEqualToTransform(_videoInputTransform, CGAffineTransformMakeRotation(M_PI))) {
        orientation = UIImageOrientationDown;
    }
    UIImage *image = [UIImage imageWithCGImage:quartzImage scale:4.0 orientation:orientation];
    
    CGImageRelease(quartzImage);
    
    return (image);
}

@end
