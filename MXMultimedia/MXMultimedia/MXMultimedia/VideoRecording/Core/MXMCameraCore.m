//
//  MXMCameraCore.m
//  MXMultimedia
//
//  Created by Qingfeng on 16/9/24.
//  Copyright © 2016年 Qingfeng. All rights reserved.
//

#import "MXMCameraCore.h"
#import "MXMMovieWriter.h"
#import "MXMMediaDataManager+FileManaging.h"
#import "MXMMultimediaTool.h"

static const NSInteger KCameraErrorCodeNotAuthorized = -11852;

static CGAffineTransform GetTransformForDeviceOrientation(UIDeviceOrientation orientation, BOOL mirrored) {
    CGAffineTransform result;
    switch (orientation) {
        case UIDeviceOrientationPortrait:
        case UIDeviceOrientationFaceUp:
        case UIDeviceOrientationFaceDown:
            result = CGAffineTransformMakeRotation(M_PI_2);
            break;
        case UIDeviceOrientationPortraitUpsideDown:
            result = CGAffineTransformMakeRotation((3 * M_PI_2));
            break;
        case UIDeviceOrientationLandscapeLeft:
            result = mirrored ?  CGAffineTransformMakeRotation(M_PI) : CGAffineTransformIdentity;
            break;
        default:
            result = mirrored ? CGAffineTransformIdentity : CGAffineTransformMakeRotation(M_PI);
            break;
    }
    return result;
}

@interface MXMCameraCore()<AVCaptureVideoDataOutputSampleBufferDelegate, AVCaptureAudioDataOutputSampleBufferDelegate, MXMMovieWriterDelegate, AVCaptureFileOutputRecordingDelegate>
{
    dispatch_queue_t _cameraQueue;
    CMSampleBufferRef _currentVideoFrameBuffer;
}

@property (nonatomic, weak) id<MXMCameraCoreDelegate> delegate;

/** Session **/
@property (nonatomic, strong) AVCaptureSession *session;

/** Video Input **/
@property (nonatomic, strong) AVCaptureDevice  *videoDevice;
@property (nonatomic, strong) AVCaptureDeviceInput *videoInput;

/** Audio Input **/
@property (nonatomic, strong) AVCaptureDevice  *audioDevice;
@property (nonatomic, strong) AVCaptureDeviceInput *audioInput;

/** Output **/
@property (nonatomic, strong) AVCaptureVideoDataOutput  *videoDataOutput;
@property (nonatomic, strong) AVCaptureAudioDataOutput  *audioDataOutput;

/** AssetWriter **/
@property (nonatomic, strong) MXMMovieWriter *movieWriter;

@property (nonatomic, strong) AVCaptureFileOutput *captureFile;
@property (nonatomic, assign) BOOL useAVCaptureFile;


@end

@implementation MXMCameraCore

- (void)dealloc
{
    if (_currentVideoFrameBuffer)
    {
        CFRelease(_currentVideoFrameBuffer);
        _currentVideoFrameBuffer = NULL;
    }
    
    [_session removeInput:self.audioInput];
    [_session removeInput:self.videoInput];
    [_session removeOutput:self.audioDataOutput];
    [_session removeOutput:self.videoDataOutput];
}

- (id) initWithDelegate:(id<MXMCameraCoreDelegate>)delegate
{
    self = [super init];
    if (self)
    {
        _currentVideoFrameBuffer = NULL;
        self.delegate = delegate;
        _cameraQueue = dispatch_queue_create("cameracore_queue", DISPATCH_QUEUE_SERIAL);
        [self setupCaptureSession];
        if (!self.useAVCaptureFile) {
            [self setupMovieWriter];
        }
    }
    return self;
}

- (id) initWithDelegate:(id<MXMCameraCoreDelegate>)delegate useAVFoundationCapture:(BOOL)useAVCapture
{
    self = [super init];
    if (self)
    {
        self.useAVCaptureFile = useAVCapture;
        return [self initWithDelegate:delegate];
    }
    return self;
}

- (void) sessionStartup
{
    if (![self.session isRunning]) {
        self.torchMode = AVCaptureTorchModeOff;
        [self.session startRunning];
    }
}

- (void) sessionShutdown
{
    [self.session stopRunning];
}
- (BOOL)isRecording
{
    return self.useAVCaptureFile ? self.captureFile.isRecording : self.movieWriter.isRecording;
}
- (void) startRecording
{
    if (self.useAVCaptureFile) {
        NSURL *fileURL = [NSURL URLWithString:[[MXMMediaDataManager defaultManager] randomMoviePath]];
        [self.captureFile startRecordingToOutputFileURL:fileURL recordingDelegate:self];
    } else {
        // TODO: set static device orientation
        UIDeviceOrientation orientation = UIDeviceOrientationPortrait;
        CGAffineTransform transform = CGAffineTransformIdentity;
        if (_videoDevice.position == AVCaptureDevicePositionFront)
            transform = GetTransformForDeviceOrientation(orientation, YES);
        else
            transform = GetTransformForDeviceOrientation(orientation, NO);
        
        self.movieWriter.videoInputTransform = transform;
        
        [self.movieWriter startTakeMovieWithPath:[[MXMMediaDataManager defaultManager] randomMoviePath]];
    }
}
- (void) stopRecording
{
    if (self.useAVCaptureFile) {
        [self.captureFile stopRecording];
    } else {
        [self.movieWriter stopTakeMovie];
    }
}
- (void) cancelRecording
{
    if (self.useAVCaptureFile) {
        ;
    } else {
        [self.movieWriter cancelTakeMovie];
    }
}

- (AVCaptureSession*)captureSession
{
    return self.session;
}
- (BOOL)setupCaptureSession
{
    AVCaptureSession *session = [[AVCaptureSession alloc] init];
    self.session = session;
    [session beginConfiguration];
    
    NSError *error = nil;
    _devicePosition = AVCaptureDevicePositionBack;
    self.videoDevice = [self cameraWithPosition:_devicePosition];
    self.videoInput = [AVCaptureDeviceInput deviceInputWithDevice:self.videoDevice error:&error];
    
    if (error)
    {
        if (error.code == KCameraErrorCodeNotAuthorized)
        {
            if ([self.delegate respondsToSelector:@selector(cameraCore:videoInitError:)]) {
                [self.delegate cameraCore:self videoInitError:error];
            }
            return NO;
        }
    }
    
    if ([self.session canAddInput:self.videoInput])
    {
        [self.session addInput:self.videoInput];
    }
    
    self.audioDevice = [AVCaptureDevice defaultDeviceWithMediaType:AVMediaTypeAudio];
    self.audioInput = [AVCaptureDeviceInput deviceInputWithDevice:self.audioDevice error:&error];
    
    if (error)
    {
        if (error.code == KCameraErrorCodeNotAuthorized)
        {
            if ([self.delegate respondsToSelector:@selector(cameraCore:audioInitError:)]) {
                [self.delegate cameraCore:self audioInitError:error];
            }
            return NO;
        }
    }
    
    if ([self.session canAddInput:self.audioInput])
    {
        [self.session addInput:self.audioInput];
    }
    
    if (self.useAVCaptureFile) {
        AVCaptureMovieFileOutput *captureFile = [[AVCaptureMovieFileOutput alloc] init];
        if ([self.session canAddOutput:captureFile])
        {
            [self.session addOutput:captureFile];
        }
        self.captureFile = captureFile;
    } else {
        /** AudioOutput **/
        AVCaptureAudioDataOutput *audioDataOutput = [[AVCaptureAudioDataOutput alloc] init];
        self.audioDataOutput = audioDataOutput;
        if ([self.session canAddOutput:self.audioDataOutput])
        {
            [self.session addOutput:self.audioDataOutput];
        }
        
        [self.audioDataOutput setSampleBufferDelegate:self queue:_cameraQueue];
        
        /** VideoOutput **/
        AVCaptureVideoDataOutput *videoDataOutput = [[AVCaptureVideoDataOutput alloc] init];
        self.videoDataOutput = videoDataOutput;
        
        NSDictionary *videoOutputOptions = [NSDictionary dictionaryWithObject:[NSNumber numberWithInt:kCVPixelFormatType_32BGRA] forKey:(id)kCVPixelBufferPixelFormatTypeKey];
        [self.videoDataOutput setVideoSettings:videoOutputOptions];
        self.videoDataOutput.alwaysDiscardsLateVideoFrames = NO;
        if ([self.session canAddOutput:self.videoDataOutput])
        {
            [self.session addOutput:self.videoDataOutput];
        }
        [self.videoDataOutput setSampleBufferDelegate:self queue:_cameraQueue];
    }
    
    /** FrameSetting **/
    AVCaptureConnection *connection = [self videoCaptureConnection];
    [connection setVideoOrientation:AVCaptureVideoOrientationLandscapeRight];
    if ([self.videoDevice lockForConfiguration:nil])
    {
        self.videoDevice.activeVideoMinFrameDuration = CMTimeMake(1, 20);
        self.videoDevice.activeVideoMaxFrameDuration = CMTimeMake(1, 25);
        
        [self.videoDevice unlockForConfiguration];
    }
    
    /** PresetSetting **/
    [self setSessionDefaultPreset];
    
    [self.session commitConfiguration];
    
    /** PreviewLayer **/
    _previewLayer = [AVCaptureVideoPreviewLayer layerWithSession:self.session];
    _previewLayer.frame = CGRectMake(0, 0, CGRectGetWidth([UIScreen mainScreen].bounds), CGRectGetHeight([UIScreen mainScreen].bounds));
    _previewLayer.videoGravity = AVLayerVideoGravityResizeAspectFill;
    _previewLayer.connection.videoOrientation = AVCaptureVideoOrientationPortrait;
    
    //    [self.session startRunning];
    
    return YES;
}

- (void)setSessionDefaultPreset
{
    /*if ([self.session canSetSessionPreset:AVCaptureSessionPreset1920x1080])
     {
     [self.session setSessionPreset:AVCaptureSessionPreset1920x1080];
     }
     else */if ([self.session canSetSessionPreset:AVCaptureSessionPresetiFrame1280x720])
     {
         [self.session setSessionPreset:AVCaptureSessionPresetiFrame1280x720];
     }
     else if ([self.session canSetSessionPreset:AVCaptureSessionPreset640x480])
     {
         [self.session setSessionPreset:AVCaptureSessionPreset640x480];
     }
     else
     {
         [self.session setSessionPreset:AVCaptureSessionPresetHigh];
     }
}
- (void)setupMovieWriter
{
    MXMMovieWriter *writer = [[MXMMovieWriter alloc] initWithDelegate:self];
    self.movieWriter = writer;
}

#pragma mark - DataOutput Delegate video & audio

- (void)captureOutput:(AVCaptureOutput *)captureOutput didOutputSampleBuffer:(CMSampleBufferRef)sampleBuffer fromConnection:(AVCaptureConnection *)connection
{
    BOOL isVideo = (captureOutput == self.videoDataOutput);
    
    if (isVideo) {
        [self handleVideoSampleBuffer:sampleBuffer];
    }
    
    [self.movieWriter processFrame:sampleBuffer isVideo:isVideo];
    
    if (self.movieWriter.isRecording && isVideo) {
        dispatch_async(dispatch_get_main_queue(), ^{
            if ([self.delegate respondsToSelector:@selector(cameraCore:recordingDuration:)]) {
                [self.delegate cameraCore:self recordingDuration:self.movieWriter.duration];
            }
        });
    }
}
- (void)captureOutput:(AVCaptureOutput *)captureOutput didDropSampleBuffer:(CMSampleBufferRef)sampleBuffer fromConnection:(AVCaptureConnection *)connection
{
    CFArrayRef attachmentsArray = CMSampleBufferGetSampleAttachmentsArray(sampleBuffer, NO);
    NSArray *attachments = (__bridge NSArray *)(attachmentsArray);
    if (attachments.count) {
        for (NSDictionary *dict in attachments) {
            id value = [dict valueForKey:(NSString *)kCMSampleBufferAttachmentKey_DroppedFrameReasonInfo];
            if (value)
            {
                NSLog(@"Failed Reason %@", value);
            }
        }
    }
}

- (void)handleVideoSampleBuffer:(CMSampleBufferRef)sampleBuffer
{
    CFRetain(sampleBuffer);
    dispatch_async(_cameraQueue, ^{
        if (_currentVideoFrameBuffer != NULL)
        {
            CFRelease(_currentVideoFrameBuffer);
            _currentVideoFrameBuffer = NULL;
        }
        _currentVideoFrameBuffer = sampleBuffer;
        CFRetain(_currentVideoFrameBuffer);
        CFRelease(sampleBuffer);
    });
}

- (AVCaptureDevice *) cameraWithPosition:(AVCaptureDevicePosition) position
{
    NSArray *devices = [AVCaptureDevice devicesWithMediaType:AVMediaTypeVideo];
    if (devices.count == 0) {
        return nil;
    }
    AVCaptureDevice *device = nil;
    for (device in devices)
    {
        if ([device position] == position)
        {
            return device;
        }
    }
    if (!device)
    {
        return [devices objectAtIndex:0];
    }
    return nil;
}

- (AVCaptureConnection *)videoCaptureConnection
{
    for (AVCaptureConnection *connection in [self.videoDataOutput connections] )
    {
        for ( AVCaptureInputPort *port in [connection inputPorts] )
        {
            if ( [[port mediaType] isEqual:AVMediaTypeVideo] )
            {
                return connection;
            }
        }
    }
    
    return nil;
}

- (AVCaptureConnection *)audioCaptureConnection
{
    for (AVCaptureConnection *connection in [self.audioDataOutput connections] )
    {
        for ( AVCaptureInputPort *port in [connection inputPorts] )
        {
            if ( [[port mediaType] isEqual:AVMediaTypeAudio])
            {
                return connection;
            }
        }
    }
    
    return nil;
}

#pragma mark - MXMMovieWriterDelegate
- (void)movieWriterDidFinishFileURL:(NSURL *)fileURL thumbImage:(UIImage *)thumbImage lastImage:(UIImage *)lastImage
{
    dispatch_async(dispatch_get_main_queue(), ^{
        if ([self.delegate respondsToSelector:@selector(cameraCore:finishedRecordVideoWithURL:thumbImage:lastImage:)]) {
            [self.delegate cameraCore:self finishedRecordVideoWithURL:fileURL thumbImage:thumbImage lastImage:lastImage];
        }
    });
}

#pragma mark - AVCaptureFileOutputRecordingDelegate
- (void)captureOutput:(AVCaptureFileOutput *)captureOutput didFinishRecordingToOutputFileAtURL:(NSURL *)outputFileURL fromConnections:(NSArray *)connections error:(NSError *)error
{
    dispatch_async(dispatch_get_main_queue(), ^{
        if ([self.delegate respondsToSelector:@selector(cameraCore:finishedRecordVideoWithURL:thumbImage:lastImage:)]) {
            [self.delegate cameraCore:self finishedRecordVideoWithURL:outputFileURL thumbImage:nil lastImage:nil];
        }
    });
}

#pragma mark - /** camera option **/
- (void) focusAndExposeAtPoint:(CGPoint)point
{
    dispatch_async(_cameraQueue, ^{
        AVCaptureDevice *device = self.videoDevice;
        NSError *error = nil;
        AVCaptureFocusMode focusMode = AVCaptureFocusModeAutoFocus;
        AVCaptureExposureMode exposureMode = AVCaptureExposureModeAutoExpose;
        BOOL monitorSubjectAreaChange = YES;
        if ([device lockForConfiguration:&error])
        {
            if ([device isFocusPointOfInterestSupported] && [device isFocusModeSupported:focusMode])
            {
                [device setFocusMode:focusMode];
                [device setFocusPointOfInterest:point];
            }
            if ([device isExposurePointOfInterestSupported] && [device isExposureModeSupported:exposureMode])
            {
                [device setExposureMode:exposureMode];
                [device setExposurePointOfInterest:point];
            }
            [device setSubjectAreaChangeMonitoringEnabled:monitorSubjectAreaChange];
            [device unlockForConfiguration];
        }
        else
        {
            NSLog(@"focusAndExpose error %@", error);
        }
    });
}

- (void) focusAtPoint:(CGPoint)point
{
    if ([self.videoDevice isFocusPointOfInterestSupported] && [self.videoDevice isFocusModeSupported:AVCaptureFocusModeAutoFocus])
    {
        NSError *error;
        if ([self.videoDevice lockForConfiguration:&error])
        {
            [self.videoDevice setFocusPointOfInterest:point];
            [self.videoDevice setFocusMode:AVCaptureFocusModeAutoFocus];
            [self.videoDevice unlockForConfiguration];
        }
    }
}

- (void) exposureAtPoint:(CGPoint)point
{
    if ([self.videoDevice isExposurePointOfInterestSupported] &&
        [self.videoDevice isExposureModeSupported:AVCaptureExposureModeContinuousAutoExposure])
    {
        NSError *error = nil;
        if ([self.videoDevice lockForConfiguration:&error])
        {
            [self.videoDevice setExposurePointOfInterest:point];
            [self.videoDevice setExposureMode:AVCaptureExposureModeContinuousAutoExposure];
            [self.videoDevice unlockForConfiguration];
        }
    }
}

- (void) autofocus
{
    AVCaptureDevice *device = [[self videoInput] device];
    if (device.focusMode == AVCaptureFocusModeContinuousAutoFocus)
    {
        return;
    }
    if ([self.videoDevice isFocusModeSupported:AVCaptureFocusModeContinuousAutoFocus])
    {
        NSError *error = nil;
        if ([self.videoDevice lockForConfiguration:&error])
        {
            [self.videoDevice setFocusMode:AVCaptureFocusModeContinuousAutoFocus];
            [self.videoDevice unlockForConfiguration];
        }
    }
}

- (void) autoExposure
{
    AVCaptureDevice *device = [[self videoInput] device];
    if (device.exposureMode == AVCaptureExposureModeContinuousAutoExposure)
    {
        return;
    }
    
    if ([self.videoDevice isExposureModeSupported:AVCaptureExposureModeContinuousAutoExposure])
    {
        NSError *error = nil;
        if ([self.videoDevice lockForConfiguration:&error])
        {
            [self.videoDevice setExposureMode:AVCaptureExposureModeContinuousAutoExposure];
            [self.videoDevice unlockForConfiguration];
        }
    }
}

- (BOOL) hasFrontCamera
{
    NSArray *array = [AVCaptureDevice devicesWithMediaType:AVMediaTypeVideo];
    if (array && [array count] == 1)
    {
        return NO;
    }
    return YES;
}

- (BOOL) hasFlash
{
    return [self.videoDevice hasTorch] && self.videoDevice.torchAvailable;
}

- (void) setTorchMode:(AVCaptureTorchMode)torchMode
{
    if ([self.videoDevice isTorchModeSupported:torchMode] && [self.videoDevice torchMode] != torchMode)
    {
        NSError *error;
        if ([self.videoDevice lockForConfiguration:&error])
        {
            [self.videoDevice setTorchMode:torchMode];
            [self.videoDevice unlockForConfiguration];
        }
    }
}

- (void) setDevicePosition:(AVCaptureDevicePosition)devicePosition
{
    if (_devicePosition == devicePosition || self.isRecording)
    {
        return;
    }
    
    [self.session beginConfiguration];
    
    AVCaptureDevice *videoDevice_ = [self cameraWithPosition:devicePosition];
    AVCaptureDeviceInput *videoInput_ = [AVCaptureDeviceInput deviceInputWithDevice:videoDevice_ error:nil];
    
    if (![videoDevice_ supportsAVCaptureSessionPreset:self.session.sessionPreset]) {
        NSString *devicePreset = nil;
        /*if ([videoDevice_ supportsAVCaptureSessionPreset:AVCaptureSessionPreset1920x1080]){
         devicePreset = AVCaptureSessionPreset1920x1080;
         }else */if([videoDevice_ supportsAVCaptureSessionPreset:AVCaptureSessionPresetiFrame1280x720]){
             devicePreset = AVCaptureSessionPresetiFrame1280x720;
         }else{
             devicePreset = AVCaptureSessionPreset640x480;
         }
        [self.session setSessionPreset:devicePreset];
    }
    
    [self.session removeInput:[self videoInput]];
    if([self.session canAddInput:videoInput_])
    {
        [self.session addInput:videoInput_];
        [self setSessionDefaultPreset];
        self.videoDevice = videoDevice_;
        self.videoInput = videoInput_;
        _devicePosition = devicePosition;
    }else{
        [self.session addInput:[self videoInput]];
    }
    
    [self.session commitConfiguration];
}

- (UIImage *) currentPreviewImage
{
    __block UIImage *image = nil;
    if (_currentVideoFrameBuffer) {
        CFRetain(_currentVideoFrameBuffer);
        dispatch_sync(_cameraQueue, ^{
            image = [MXMMultimediaTool imageFromSampleBuffer:_currentVideoFrameBuffer];
            CFRelease(_currentVideoFrameBuffer);
        });
    }
    return image;
}

@end
