//
//  MXMCameraCore.h
//  MXMultimedia
//
//  Created by Qingfeng on 16/9/24.
//  Copyright © 2016年 Qingfeng. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <AVFoundation/AVFoundation.h>

@protocol MXMCameraCoreDelegate;

@interface MXMCameraCore : NSObject

@property (nonatomic, strong, readonly) AVCaptureSession *captureSession;
@property (nonatomic, strong, readonly) AVCaptureVideoPreviewLayer *previewLayer;

@property (nonatomic, assign, readonly) BOOL isRecording;
@property (nonatomic, assign) AVCaptureTorchMode torchMode;
@property (nonatomic, assign) AVCaptureDevicePosition devicePosition;

- (id) initWithDelegate:(id<MXMCameraCoreDelegate>)delegate;
- (id) initWithDelegate:(id<MXMCameraCoreDelegate>)delegate useAVFoundationCapture:(BOOL)useAVCapture;

- (void) sessionStartup;
- (void) sessionShutdown;

- (void) startRecording;
- (void) stopRecording;
- (void) cancelRecording;

/** camera option **/
- (void) focusAndExposeAtPoint:(CGPoint)point;
- (void) focusAtPoint:(CGPoint)point;
- (void) exposureAtPoint:(CGPoint)point;
- (void) autofocus;
- (void) autoExposure;
- (BOOL) hasFrontCamera;
- (BOOL) hasFlash;

- (UIImage *) currentPreviewImage;

@end

@protocol MXMCameraCoreDelegate <NSObject>

- (void) cameraCore:(MXMCameraCore *)camera videoInitError:(NSError *)error;
- (void) cameraCore:(MXMCameraCore *)camera audioInitError:(NSError *)error;

- (void) cameraCore:(MXMCameraCore *)camera finishedRecordVideoWithURL:(NSURL *)url;
- (void) cameraCore:(MXMCameraCore *)camera recordingDuration:(CMTime)duration;

- (void) cameraCore:(MXMCameraCore *)camera finishedRecordVideoWithURL:(NSURL *)fileURL thumbImage:(UIImage *)thumbImage lastImage:(UIImage *)lastImage;
@end
