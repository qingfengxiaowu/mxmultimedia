//
//  MXMVideoRecordingController.m
//  MXMultimedia
//
//  Created by Qingfeng on 16/9/23.
//  Copyright © 2016年 Qingfeng. All rights reserved.
//

#import "MXMVideoRecordingController.h"
#import "MXMCameraOverlayView.h"
#import "MXMCameraCore.h"
#import "MXMMediaDataManager+FileManaging.h"

@interface MXMVideoRecordingController () <
MXMCameraCoreDelegate,
MXMCameraOverlayViewDelegate>

@property (nonatomic, strong) MXMCameraCore *cameraCore;
@property (nonatomic, strong) MXMCameraOverlayView *cameraOverlayView;
@property (nonatomic) BOOL canAccessCamera;
@property (nonatomic) CGFloat minDuration;
@property (nonatomic) CGFloat maxDuration;

@end

@implementation MXMVideoRecordingController

#pragma mark - Life Cycle

- (instancetype)initWithMinDuration:(CGFloat)minDuration maxDuration:(CGFloat)maxDuration {
    self = [super init];
    if (self) {
        _minDuration = minDuration;
        _maxDuration = maxDuration;
        _canAccessCamera = YES;
    }
    return self;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    
    [self configureCustomViews];
    [self setupCameraCore];
}

- (BOOL)prefersStatusBarHidden {
    return YES;
}

#pragma mark - CameraCoreDelegate

- (void)cameraCore:(MXMCameraCore *)camera finishedRecordVideoWithURL:(NSURL *)fileURL thumbImage:(UIImage *)thumbImage lastImage:(UIImage *)lastImage {
    NSString *fileName = [fileURL lastPathComponent];
    MXMVideoItem *videoItem = [[MXMVideoItem alloc] initWithFileName:fileName];
    [[MXMMediaDataManager defaultManager] addVideoItem:videoItem];
    [self updateCompletedProgress];
}

- (void)cameraCore:(MXMCameraCore *)camera recordingDuration:(CMTime)duration {
    CGFloat currentDuration = CMTimeGetSeconds(duration);
    [self.cameraOverlayView updateWithCurrentDuration:currentDuration];
}

- (void)cameraCore:(MXMCameraCore *)camera videoInitError:(NSError *)error {
    self.canAccessCamera = NO;
}

- (void)cameraCore:(MXMCameraCore *)camera audioInitError:(NSError *)error {
    self.canAccessCamera = NO;
}

- (void)cameraCore:(MXMCameraCore *)camera finishedRecordVideoWithURL:(NSURL *)url {
}

#pragma mark - MXMCameraOverlayViewDelegate

- (void)didToggleCameraSwitching:(MXMCameraOverlayView *)overlayView {
    if (self.cameraCore.isRecording) {
        return;
    }
    
    if (self.cameraCore.devicePosition == AVCaptureDevicePositionBack) {
        self.cameraCore.devicePosition = AVCaptureDevicePositionFront;
    }
    else{
        self.cameraCore.devicePosition = AVCaptureDevicePositionBack;
    }
}

- (void)didToggleVideoDeletion:(MXMCameraOverlayView *)overlayView {
    [[MXMMediaDataManager defaultManager] removeLastVideoItem];
    [self updateCompletedProgress];
}

- (void)overlayView:(MXMCameraOverlayView *)overlayView didTapBackButton:(UIButton *)button {
    
}

- (void)overlayView:(MXMCameraOverlayView *)overlayView didTapRecordButton:(UIButton *)button {
    dispatch_async(dispatch_get_main_queue(), ^{
        if (self.cameraCore.isRecording) {
            [self.cameraCore stopRecording];
            [UIApplication sharedApplication].idleTimerDisabled = NO;
        }
        else {
            if (self.cameraCore.captureSession.running) {
                [self.cameraCore startRecording];
                [UIApplication sharedApplication].idleTimerDisabled = YES;
            }
        }
    });
}

- (void)overlayView:(MXMCameraOverlayView *)overlayView didTapVideoAdditionButton:(UIButton *)button {
    
}

- (void)overlayView:(MXMCameraOverlayView *)overlayView didTapcompletionButton:(UIButton *)button {
    
}

#pragma mark - Private

- (void)configureCustomViews {
    self.view.backgroundColor = [UIColor whiteColor];
    self.edgesForExtendedLayout = UIRectEdgeNone;
    
    [self.view addSubview:self.cameraOverlayView];
    [self.cameraOverlayView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.edges.equalTo(self.view);
    }];
    
    [self updateCompletedProgress];
}

- (void)setupCameraCore {
    if ([self needsRequestMediaAuthorization]) {
        [AVCaptureDevice requestAccessForMediaType:AVMediaTypeVideo completionHandler:^(BOOL granted){
            if (NO == granted) {
                return ;
            }
            
            [AVCaptureDevice requestAccessForMediaType:AVMediaTypeAudio completionHandler:^(BOOL granted){
                if (NO == granted) {
                    return ;
                }
                
                [self performSelectorOnMainThread:@selector(loadCameraCore) withObject:nil waitUntilDone:YES];
            }];
        }];
    }
    else {
        [self loadCameraCore];
    }
}

- (void)loadCameraCore {
    self.cameraCore = [[MXMCameraCore alloc] initWithDelegate:self useAVFoundationCapture:NO];
    AVCaptureVideoPreviewLayer *previewLayer = self.cameraCore.previewLayer;
    CGRect bounds = [UIScreen mainScreen].bounds;
    previewLayer.frame = CGRectMake(0, 0, CGRectGetWidth(bounds), CGRectGetHeight(bounds));
    [self.view.layer addSublayer:previewLayer];
    
    [self startUpCamera];
    [self.view bringSubviewToFront:self.cameraOverlayView];
}

- (void)startUpCamera {
    if (self.canAccessCamera) {
        [self.cameraCore sessionStartup];
    }
}

- (void)shutdownCamera {
    if (self.canAccessCamera) {
        [self.cameraCore sessionShutdown];
    }
}

- (BOOL)needsRequestMediaAuthorization {
    AVAuthorizationStatus status = [AVCaptureDevice authorizationStatusForMediaType:AVMediaTypeVideo];
    return AVAuthorizationStatusNotDetermined == status;
}

- (void)updateCompletedProgress {
    [self.cameraOverlayView updateWithCompletedDurationValues:[self completedDurationValues]];
}

- (NSArray<NSNumber *> *)completedDurationValues {
    NSArray<MXMVideoItem *> *videoItems = [MXMMediaDataManager defaultManager].videoItems;
    NSMutableArray *completedDurationValues = [NSMutableArray array];
    for (MXMVideoItem *videoItem in videoItems) {
        [completedDurationValues addObject:@([videoItem validDuration])];
    }
    return [completedDurationValues copy];
}

#pragma mark - Getters & Setters

- (MXMCameraOverlayView *)cameraOverlayView {
    if (_cameraOverlayView) {
        return _cameraOverlayView;
    }
    
    _cameraOverlayView = [[MXMCameraOverlayView alloc] initWithMinDuration:self.minDuration
                                                               maxDuration:self.maxDuration];
    _cameraOverlayView.delegate = self;
    return _cameraOverlayView;
}

@end
