//
//  MXMVideoRecordingController.h
//  MXMultimedia
//
//  Created by Qingfeng on 16/9/23.
//  Copyright © 2016年 Qingfeng. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface MXMVideoRecordingController : UIViewController

- (instancetype)initWithMinDuration:(CGFloat)minDuration maxDuration:(CGFloat)maxDuration;

@end
