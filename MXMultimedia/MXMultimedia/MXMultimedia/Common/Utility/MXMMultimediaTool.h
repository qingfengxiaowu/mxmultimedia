//
//  MXMMultimediaTool.h
//  MXMultimedia
//
//  Created by Qingfeng on 16/9/23.
//  Copyright © 2016年 Qingfeng. All rights reserved.
//

#import <CoreMedia/CoreMedia.h>

@interface MXMMultimediaTool : NSObject

+ (NSString *)formatMediaDuration:(CGFloat)duration;

+ (UIImage *)imageFromSampleBuffer:(CMSampleBufferRef)sampleBuffer;

@end
