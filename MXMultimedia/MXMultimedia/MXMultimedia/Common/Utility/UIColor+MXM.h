//
//  UIColor+MXM.h
//  MXMultimedia
//
//  Created by Qingfeng on 16/9/24.
//  Copyright © 2016年 Qingfeng. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UIColor (MXM)

+ (UIColor * _Nullable)mxm_colorWithHexString:(NSString * _Nonnull)hexString;

+ (UIColor * _Nullable)mxm_colorWithHexString:(NSString * _Nonnull)hexString alpha:(CGFloat)alpha;

@end
