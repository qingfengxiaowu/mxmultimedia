//
//  UIColor+MXM.m
//  MXMultimedia
//
//  Created by Qingfeng on 16/9/24.
//  Copyright © 2016年 Qingfeng. All rights reserved.
//

#import "UIColor+MXM.h"

@implementation UIColor (MXM)

+ (UIColor * _Nullable)mxm_colorWithHexString:(NSString * _Nonnull)hexString {
    if ([hexString hasPrefix:@"#"]) {
        hexString = [hexString substringFromIndex:1];
    }
    if ([[hexString lowercaseString] hasPrefix:@"0x"]) {
        hexString = [hexString substringFromIndex:2];
    }
    if ([hexString length] != 6) {
        return nil;
    }
    
    NSScanner *scanner = [[NSScanner alloc] initWithString:hexString];
    unsigned hexValue = 0;
    if ([scanner scanHexInt:&hexValue] && [scanner isAtEnd]) {
        int r = ((hexValue & 0xFF0000) >> 16);
        int g = ((hexValue & 0x00FF00) >>  8);
        int b = ( hexValue & 0x0000FF)       ;
        return [self colorWithRed:((float)r / 255)
                            green:((float)g / 255)
                             blue:((float)b / 255)
                            alpha:1.0];
    }
    
    return nil;
}

+ (UIColor * _Nullable)mxm_colorWithHexString:(NSString * _Nonnull)hexString alpha:(CGFloat)alpha {
    return [[self mxm_colorWithHexString:hexString] colorWithAlphaComponent:alpha];
}

@end
