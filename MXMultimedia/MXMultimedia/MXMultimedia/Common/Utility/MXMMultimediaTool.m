//
//  MXMMultimediaTool.m
//  MXMultimedia
//
//  Created by Qingfeng on 16/9/23.
//  Copyright © 2016年 Qingfeng. All rights reserved.
//

#import "MXMMultimediaTool.h"

static const NSInteger MXMSecondsPerMinute = 60;
static const NSInteger MXMSecondsPerHour = 3600;

@implementation MXMMultimediaTool

+ (NSString *)formatMediaDuration:(CGFloat)duration {
    if (duration <= 0) {
        return @"00:00";
    }
    
    NSInteger hours = duration / MXMSecondsPerHour;
    NSInteger minutes = (duration - hours * MXMSecondsPerHour) / MXMSecondsPerMinute;
    CGFloat seconds = duration - hours * MXMSecondsPerHour - minutes * MXMSecondsPerMinute;
    if (seconds > 9.94 && seconds < 10.0) {
        seconds = 10.0;
    }
    NSString *secondsString = (seconds < 10) ? [NSString stringWithFormat:@"0%.1f", seconds] : [NSString stringWithFormat:@"%.1f", seconds];
    
    NSString *formatString = nil;
    if (hours > 0) {
        formatString = [NSString stringWithFormat:@"%.02zd:%.02zd:%@", hours, minutes, secondsString];
    }
    else {
        formatString = [NSString stringWithFormat:@"%.02zd:%@", minutes, secondsString];
    }
    return formatString;
}

+ (UIImage *)imageFromSampleBuffer:(CMSampleBufferRef)sampleBuffer {
    CVImageBufferRef imageBuffer = CMSampleBufferGetImageBuffer(sampleBuffer);
    CVPixelBufferLockBaseAddress(imageBuffer, 0);
    
    void *baseAddress = CVPixelBufferGetBaseAddress(imageBuffer);
    size_t bytesPerRow = CVPixelBufferGetBytesPerRow(imageBuffer);
    size_t width = CVPixelBufferGetWidth(imageBuffer);
    size_t height = CVPixelBufferGetHeight(imageBuffer);
    
    CGColorSpaceRef colorSpace = CGColorSpaceCreateDeviceRGB();
    CGContextRef context = CGBitmapContextCreate(baseAddress, width, height, 8, bytesPerRow, colorSpace, kCGBitmapByteOrder32Little | kCGImageAlphaPremultipliedFirst);
    CGImageRef quartzImage = CGBitmapContextCreateImage(context);
    CVPixelBufferUnlockBaseAddress(imageBuffer,0);
    
    CGContextRelease(context);
    CGColorSpaceRelease(colorSpace);
    
    UIImage *image = [UIImage imageWithCGImage:quartzImage scale:8.0 orientation:UIImageOrientationUp];
    
    CGImageRelease(quartzImage);
    return (image);
}

@end
