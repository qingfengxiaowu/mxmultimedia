//
//  MXMVideoItem.h
//  QFMultimedia
//
//  Created by Qingfeng on 16/9/20.
//  Copyright © 2016年 Qingfeng. All rights reserved.
//

#import <CoreMedia/CoreMedia.h>
#import <UIKit/UIKit.h>
#import <YYModel/YYModel.h>

NS_ASSUME_NONNULL_BEGIN

@interface MXMVideoItem : NSObject <YYModel>

@property (nonatomic, copy, nullable) NSString *fileName;
@property (nonatomic) CGFloat widthHeightRatio; // Defaults to 9.0 / 16.0
@property (nonatomic) CMTimeRange validTimeRange;
@property (nonatomic) CMTimeRange editableTimeRange;
@property (nonatomic) CGSize sourceSize;
@property (nonatomic) CGRect cropRect;

- (instancetype)initWithFileName:(NSString *)fileName;
- (void)configureVideoInfo;
- (NSString *)filePath;
- (CGFloat)validStart;
- (CGFloat)validDuration;

@end

NS_ASSUME_NONNULL_END
