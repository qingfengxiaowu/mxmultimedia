//
//  MXMMediaDataManager.m
//  MXMultimedia
//
//  Created by Qingfeng on 16/9/23.
//  Copyright © 2016年 Qingfeng. All rights reserved.
//

#import "MXMMediaDataManager.h"
#import "MXMMediaDataManager+FileManaging.h"

@interface MXMMediaDataManager ()

@property (nonatomic, strong) NSMutableArray<MXMVideoItem *> *privateVideoItems;

@end

@implementation MXMMediaDataManager

@dynamic videoItems;

#pragma mark - Life Cycle

+ (instancetype)defaultManager {
    static id sharedInstance = nil;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        sharedInstance = [[self alloc] init];
    });
    return sharedInstance;
}

#pragma mark - Private

- (void)setup {
    dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
        [self loadVideoItemsFromLocal];
    });
}

#pragma mark - Data Management

- (void)addVideoItem:(MXMVideoItem *)videoItem {
    @synchronized (self) {
        if (videoItem) {
            [self.privateVideoItems addObject:videoItem];
        }
    }
    [self saveVideoItems];
}

- (void)removeLastVideoItem {
    @synchronized (self) {
        [self.privateVideoItems removeLastObject];
    }
    [self saveVideoItems];
}

- (void)removeAllVideoItems {
    @synchronized (self) {
        [self.privateVideoItems removeAllObjects];
    }
    [self saveVideoItems];
}

- (void)saveVideoItems {
    NSArray *videoItems = self.videoItems;
    NSMutableArray *videoObjects = [NSMutableArray array];
    for (MXMVideoItem *videoItem in videoItems) {
        id videoObject = [videoItem yy_modelToJSONObject];
        if (videoObject) {
            [videoObjects addObject:videoObject];
        }
    }
    [videoObjects writeToFile:self.videoInfoFilePath atomically:YES];
}

- (void)loadVideoItemsFromLocal {
    NSArray *videoObjects = [NSArray arrayWithContentsOfFile:self.videoInfoFilePath];
    NSMutableArray *videoItems = [NSMutableArray array];
    for (id videoObject in videoObjects) {
        MXMVideoItem *videoItem = [MXMVideoItem yy_modelWithJSON:videoObject];
        if (videoItem) {
            [videoItem configureVideoInfo];
            [videoItems addObject:videoItem];
        }
    }
    self.privateVideoItems = videoItems;
}

#pragma mark - Getters & Setters

- (NSArray<MXMVideoItem *> *)videoItems {
    NSArray *videoItems = nil;
    @synchronized (self) {
        videoItems = [self.privateVideoItems copy];
    }
    return videoItems;
}

@end
