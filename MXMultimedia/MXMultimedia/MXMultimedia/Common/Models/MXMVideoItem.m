//
//  MXMVideoItem.m
//  QFMultimedia
//
//  Created by Qingfeng on 16/9/20.
//  Copyright © 2016年 Qingfeng. All rights reserved.
//

#import "MXMVideoItem.h"
#import <AVFoundation/AVFoundation.h>
#import "MXMMediaDataManager+FileManaging.h"

@implementation MXMVideoItem

#pragma mark - Life Cycle

- (instancetype)initWithFileName:(NSString *)fileName {
    self = [super init];
    if (self) {
        _fileName = fileName;
        _widthHeightRatio = 9.0 / 16.0;
        [self configureVideoInfo];
    }
    return self;
}

- (void)configureVideoInfo {
    NSURL *videoFileURL = [NSURL fileURLWithPath:[self filePath]];
    AVURLAsset *asset = [AVURLAsset URLAssetWithURL:videoFileURL options:nil];
    self.editableTimeRange = CMTimeRangeMake(kCMTimeZero, asset.duration);
    self.validTimeRange = self.editableTimeRange;
    if ([asset tracksWithMediaType:AVMediaTypeVideo].count) {
        AVAssetTrack *assetVideoTrack = [asset tracksWithMediaType:AVMediaTypeVideo][0];
        CGAffineTransform assetTransform = assetVideoTrack.preferredTransform;
        CGSize realSize = CGSizeApplyAffineTransform(assetVideoTrack.naturalSize, assetTransform);
        self.sourceSize = CGSizeMake(fabs(realSize.width), fabs(realSize.height));
        self.cropRect = [self clipRectWithSize:self.sourceSize widthHeightRatio:self.widthHeightRatio];
    }
}

#pragma mark - Data Management

- (NSString *)filePath {
    return [[MXMMediaDataManager defaultManager].tempPath stringByAppendingPathComponent:self.fileName];
}

- (CGFloat)validStart {
    return CMTimeGetSeconds(self.validTimeRange.start);
}

- (CGFloat)validDuration {
    return CMTimeGetSeconds(self.validTimeRange.duration);
}

- (CGFloat)editableStart {
    return CMTimeGetSeconds(self.editableTimeRange.start);
}

- (CGFloat)editableDuration {
    return CMTimeGetSeconds(self.editableTimeRange.duration);
}

#pragma mark - Private

// 按照宽高比裁剪，返回Rect为{0, 0, 1, 1}模式
- (CGRect)clipRectWithSize:(CGSize)size widthHeightRatio:(CGFloat)widthHeightRatio {
    if (size.width / size.height > widthHeightRatio) {
        CGFloat newWidth = size.height * widthHeightRatio;
        CGFloat x = (size.width - newWidth) / 2.f / size.width;
        return CGRectMake(x, 0, (newWidth / size.width), 1);
    }
    else {
        CGFloat newHeight = size.width / widthHeightRatio;
        CGFloat y = (size.height - newHeight) / 2.f / size.height;
        return CGRectMake(0, y, 1, (newHeight / size.height));
    }
}

@end
