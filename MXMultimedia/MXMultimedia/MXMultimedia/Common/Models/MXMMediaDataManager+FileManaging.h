//
//  MXMMediaDataManager+FileManaging.h
//  MXMultimedia
//
//  Created by Qingfeng on 16/9/23.
//  Copyright © 2016年 Qingfeng. All rights reserved.
//

#import "MXMMediaDataManager.h"

NS_ASSUME_NONNULL_BEGIN

@interface MXMMediaDataManager (FileManaging)

@property (nonatomic, readonly, copy) NSString *rootPath;
@property (nonatomic, readonly, copy) NSString *tempPath;

@property (nonatomic, readonly, copy) NSString *filterPath;
@property (nonatomic, readonly, copy) NSString *videoInfoFilePath;

- (NSString *)randomMoviePath;

@end

NS_ASSUME_NONNULL_END
