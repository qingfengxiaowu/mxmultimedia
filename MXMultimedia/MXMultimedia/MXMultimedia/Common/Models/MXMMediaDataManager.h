//
//  MXMMediaDataManager.h
//  MXMultimedia
//
//  Created by Qingfeng on 16/9/23.
//  Copyright © 2016年 Qingfeng. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "MXMVideoItem.h"

NS_ASSUME_NONNULL_BEGIN

@interface MXMMediaDataManager : NSObject

@property (nonatomic, readonly, copy) NSArray<MXMVideoItem *> *videoItems;

+ (instancetype)defaultManager;

- (void)setup;

- (void)addVideoItem:(MXMVideoItem *)videoItem;

- (void)removeLastVideoItem;

- (void)removeAllVideoItems;

@end

NS_ASSUME_NONNULL_END
