//
//  MXMMediaDataManager+FileManaging.m
//  MXMultimedia
//
//  Created by Qingfeng on 16/9/23.
//  Copyright © 2016年 Qingfeng. All rights reserved.
//

#import "MXMMediaDataManager+FileManaging.h"

static NSString *const MXMultimediaRootDirectoryName = @"MXMultimedia";
static NSString *const MXMultimediaTempDirectoryName = @"Temp";
static NSString *const MXMultimediaFilterDirectoryName = @"filters";
static NSString *const MXMultimediaVideoInfoFileName = @"Videos.plist";

@implementation MXMMediaDataManager (FileManaging)

@dynamic rootPath, filterPath, tempPath, videoInfoFilePath;

#pragma mark - Private

- (NSString *)randomPathWithPostfix:(NSString *)postfix {
    NSDateFormatter *formatter = [NSDateFormatter new];
    [formatter setDateFormat:@"yyyy-MM-dd-HH-mm-ss"];
    NSString *datePrefix = [formatter stringFromDate:[NSDate date]];
    
    NSInteger fileNamePostfix = 0;
    NSString *filePath = nil;
    do {
        filePath =[NSString stringWithFormat:@"%@/%@-%ld.%@", self.tempPath, datePrefix, (long)fileNamePostfix++, postfix];
    } while ([[NSFileManager defaultManager] fileExistsAtPath:filePath]);
    
    return filePath;
}

- (BOOL)createDirectoryAtPath:(NSString *)path {
    if (!path.length) {
        return NO;
    }
    
    BOOL needCreateDirectory = YES;
    BOOL isDirectory = NO;
    if ([[NSFileManager defaultManager] fileExistsAtPath:path isDirectory:&isDirectory]) {
        if (isDirectory) {
            needCreateDirectory = NO;
        }
        else {
            [[NSFileManager defaultManager] removeItemAtPath:path error:nil];
        }
    }
    
    if (needCreateDirectory) {
        return [[NSFileManager defaultManager] createDirectoryAtPath:path withIntermediateDirectories:YES attributes:nil error:nil];
    }
    
    return YES;
}

- (NSString *)randomMoviePath {
    return [self randomPathWithPostfix:@"mov"];
}

#pragma mark - Getters & Setters

- (NSString *)rootPath {
    NSSearchPathDirectory mainDirectory = NSLibraryDirectory;
#if DEBUG
    mainDirectory = NSDocumentDirectory;
#endif
    NSArray *mainPaths = NSSearchPathForDirectoriesInDomains(mainDirectory, NSUserDomainMask, YES);
    NSString *mainPath = mainPaths.firstObject;
    NSString *rootDirectoryPath = [mainPath stringByAppendingPathComponent:MXMultimediaRootDirectoryName];
    [self createDirectoryAtPath:rootDirectoryPath];
    return rootDirectoryPath;
}

- (NSString *)tempPath {
    NSString *tempPath = [self.rootPath stringByAppendingPathComponent:MXMultimediaTempDirectoryName];
    [self createDirectoryAtPath:tempPath];
    return tempPath;
}

- (NSString *)filterPath {
    NSString *filterPath = [self.rootPath stringByAppendingPathComponent:MXMultimediaFilterDirectoryName];
    [self createDirectoryAtPath:filterPath];
    return filterPath;
}

- (NSString *)videoInfoFilePath {
    NSString *videoInfoFilePath = [self.rootPath stringByAppendingPathComponent:MXMultimediaVideoInfoFileName];
    return videoInfoFilePath;
}

@end
